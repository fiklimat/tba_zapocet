from fastapi import FastAPI, Request, Response, UploadFile, File, Query
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
import uvicorn
import requests
import utils as ut
from pathlib import Path

app = FastAPI(title="Web Client")
template_path = Path(__file__).parent.joinpath("templates")
templates = Jinja2Templates(directory=template_path)
config = ut.loadConfig("api_config.json")

@app.get("/", response_class=HTMLResponse, summary="Get main HTML page",)
async def read_form(request: Request):
    return templates.TemplateResponse("index.html",{"request": request})
    
@app.get("/get-client-id", summary="Get client Ids from API", responses={
    200: {
        "description": "Successful Response",
        "content": {
            "application/json": {
                "example": [
                    0
                ]
            }
        }
    }
})
async def get_client_ids():
    response = requests.get(f"{config['url']}client_id")
    return response.json()

@app.get("/get-monitoring-csv", summary="Download monitoring data from API as CSV",response_class=Response(media_type="text/csv"), responses={
    200: {
        "description": "Successful Response",
        "content": {
            "text/csv": {
                "example": """net_bytes_recv,gpu_percent,id,net_bytes_sent,gpu_mem_free,cpu_frequency,net_packets_recv,gpu_mem_total,timestamp,mem_available,net_packets_sent,gpu_mem_used,cpu_percent,mem_free,battery_percent,gpu_temp,mem_percent,battery_plugged,mem_total,batery_secsleft,client_id,mem_used
2702,0.00,1,0,5156.0,2808.00,8,6144.0,2024-05-22 23:49:31,4632,0,874.0,9.90,4632,100,53.0,73.00,True,17134,-2,1,12503"""
            }
        }
    }
})
async def get_monitoring_csv(ci: str = Query(default=None, description="Client id"), 
                             f: str = Query(default=None, description="Start datetime filter [format: Y-m-dTH:M]"), 
                             t: str = Query(default=None, description="End datetime filter [format: Y-m-dTH:M]")):
    api_response = requests.get(f"{config['url']}monitoring/csv", params={'ci': ci, 'f': f, 't': t})
    response = Response(content=api_response.content, media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=data.csv"
    response.headers["Content-Type"] = "text/csv"   
    return response

@app.post('/post-monitoring-csv', summary="Upload monitoring data in CSV to API")
async def post_monitoring_csv(csv_file: UploadFile = File(description="CSV file with monitoring data")):
    if not csv_file.filename.endswith('.csv'):
        return {"error": "Only CSV files are allowed"}
    contents = await csv_file.read()
    requests.post(f"{config['url']}monitoring/csv",files={"csv_file": (csv_file.filename, contents)})

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8006)
    # uvicorn.run(app, host="localhost", port=8006)