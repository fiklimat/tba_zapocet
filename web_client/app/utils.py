from pathlib import Path
import json

def loadConfig(name: str) -> dict:
    path = Path(__file__).parent.joinpath(f"config/{name}")
    with open(path,"r") as file:
        return json.load(file)