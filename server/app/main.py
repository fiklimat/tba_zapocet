import json
import utils as ut
from rabbit_handler import RabbitHandler
from db_handler import DBHandler
from classes import create_database

db: DBHandler

def callback(ch, method, properties, body):
    session = db.get_session()
    data = json.loads(body)
    try:
        with session.begin():
            monitoring = ut.monitoring_from_json(data)
            session.add(monitoring)
    except Exception as e:
        print(e)
        session.rollback()
    finally:
        session.close()

if __name__ == "__main__":
    rh = RabbitHandler()
    db = DBHandler()
    create_database(db)

    channel = rh.get_channel()
    channel.queue_declare(queue=rh.queue)
    channel.basic_consume(queue=rh.queue, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()