from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import utils as ut

# class for handling database conection 
class DBHandler:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(DBHandler, cls).__new__(cls)
            cls._instance.engine = create_engine(ut.loadConfig("db_config.json")['url'])
        return cls._instance
    
    def get_session(self):
        return Session(self.engine)