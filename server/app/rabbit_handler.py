import utils as ut
import pika

# class for handling rabbitMQ connection
class RabbitHandler:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(RabbitHandler, cls).__new__(cls)
            config = ut.loadConfig("rabbitmq_config.json")
            cls._instance.connection = pika.BlockingConnection(pika.ConnectionParameters(host=config['host'],port=config['port']))
            cls._instance.channel = None
            cls._instance.queue = config['queue']
        return cls._instance
    
    def get_channel(self):
        if self.channel is None:
            self.channel = self.connection.channel()
        return self.channel
    
    def get_queue(self):
        return self.queue
