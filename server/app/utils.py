from pathlib import Path
from classes import Monitoring
import json
from datetime import datetime

def loadConfig(name: str) -> dict:
    path = Path(__file__).parent.joinpath(f"config/{name}")
    with open(path,"r") as file:
        return json.load(file)
    
def monitoring_from_json(data: dict) -> Monitoring:
    return Monitoring(
        client_id = data['client_id'],
        timestamp = datetime.strptime(data['timestamp'],"%Y-%m-%d %H:%M:%S"),
        cpu_percent = data['cpu_percent'],
        cpu_frequency = data['cpu_frequency'],
        mem_available = data['mem_available'],
        mem_free = data['mem_free'],
        mem_percent = data['mem_percent'],
        mem_total = data['mem_total'],
        mem_used = data['mem_used'],
        net_bytes_recv = data['net_bytes_recv'],
        net_bytes_sent = data['net_bytes_sent'],
        net_packets_recv = data['net_packets_recv'],
        net_packets_sent = data['net_packets_sent'],
        battery_percent = data['battery_percent'],
        battery_plugged = data['battery_plugged'],
        batery_secsleft = data['batery_secsleft'],
        gpu_percent = data['gpu_percent'],
        gpu_mem_free = data['gpu_mem_free'],
        gpu_mem_total = data['gpu_mem_total'],
        gpu_mem_used = data['gpu_mem_used'],
        gpu_temp = data['gpu_temp']
)