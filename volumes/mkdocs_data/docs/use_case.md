# Use case diagramy

## Monitoring Client

![](use_case_diagram_mc.jpg)

## Grafana

![](use_case_diagram_g.jpg)

## Web Client

![](use_case_diagram_wc.jpg)