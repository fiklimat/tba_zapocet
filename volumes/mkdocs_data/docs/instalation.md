# Návod ke spuštění + odkazy

- Puštění všech komponent běžících v Dockeru příkazem:
  `docker compose up --build -d`
- Spuštění klienta na klientském zařízení, např. ve VS Code (Otestováno jen na Windows OS)

- Sledování dat dostupné na: [Grafana](http://localhost:8003/d/bdlryv7l63zeob/overview?orgId=1&refresh=1s)
  _(username: admin, password: admin)_
- Webové rozhraní pro import/export csv dat dostupné na: [Web Client](http://localhost:8006/)
- Dokumentace endpointů ve Swagger UI dostupné na: [Web Client docs](http://localhost:8006/docs#/), [API docs](http://localhost:8000/docs#/)
- Pro přmý přístup do databáze lze využít phpMyAdmin, který je dostupný na: [phpMyAdmin](http://localhost:8001/) _(username: user, password: user)_
- Zatížení rabbitMQ fronty lze sledovat na: [rabbitMQ](http://localhost:8002/) _(username: guest, password: guest)_

Databázi automaticky vytvoří API nebo Server (kdyby ne, přiložen je i db_init.sql)

# Práce s aplikací

- po spuštění dockeru se spustí monitoring clienta na cílovém zařízení, jehož výkon má být monitorován. Monitoring client zasílá naměřené hodnoty každou sekundu přes rabbitMQ na server, který je ukládá do databáze. Změny v databázi se zobrazují v Grafaně, kde lze sledovat výkon monitorovaného zařízení v reálném čase.
- import dat přes web clienta je pro zpětné prohlížení výkonu zařízení, které například nemá přístup k serveru, a tudíž nemůže zasílat data v reálném čase. Pro prohlížení takových dat by bylo možné vytvořit další dashboard v grafaně, který by zobrazoval data například za posledních x hodin (vytvořený dashboard zobrazuje pouze poslední přidané hodnoty, proto se hodí jen pro monitoraci v reálném čase).
- export dat přes web clienta může sloužit například k tomu, pokud by bylo žádoucí prohlížet naměřená v nějaké jiné aplikaci než v grafaně.

## Formát CSV souboru

Pro import nebo export dat se používá csv soubor s následujícími sloupci:

- id
- client_id
- timestamp
- cpu_percent
- cpu_frequency
- mem_available
- mem_free
- mem_percent
- mem_total
- mem_used
- net_bytes_recv
- net_bytes_sent
- net_packets_recv
- net_packets_sent
- battery_percent
- battery_plugged
- batery_secsleft
- gpu_percent
- gpu_mem_free
- gpu_mem_total
- gpu_mem_used
- gpu_temp
