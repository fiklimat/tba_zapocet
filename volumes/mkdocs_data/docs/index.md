# O aplikaci

Tato aplikace slouží pro monitorování výkonu několika zařízení v reálném čase.

## Diagram komponent

![](diagram_komponent.jpg)

## Popis komponent

### Monitoring client

Samotný klient moniturující výkon hostitelského zařízení, jako zatížení CPU, zatížení a teplotu GPU nebo zatížení sítě. Tyto data následně odesílá přes rabbitMQ.

### rabbitMQ

Zprostředkovává komunikaci mezi klienty a serverem. Umožňuje nasazení i více serverů a zabraňuje ztrátě dat i při výpadku serveru.

### Server

Konzumuje zprávy z rabbitMQ a přijatá data ukládá do databáze.

### API

API umožňuje práci s databází přes definované metody.

### Web Client

Vytváří grafické rozhraní pro export a import dat do databáze v CSV formátu

### Grafana

Vytváří grafické rozhraní pro sledování naměřených dat v reálném čase.
