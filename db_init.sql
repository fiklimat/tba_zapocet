USE zapocet_db;

CREATE TABLE `monitoring` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `client_id` int NOT NULL,
  `cpu_percent` decimal(5,2),
  `cpu_frequency` decimal(10,2),
  `mem_available` int,
  `mem_free` int,
  `mem_percent` decimal(5,2),
  `mem_total` int,
  `mem_used` int,
  `net_bytes_recv` bigint,
  `net_bytes_sent` bigint,
  `net_packets_recv` int,
  `net_packets_sent` int,
  `battery_percent` int,
  `battery_plugged` boolean,
  `batery_secsleft` int,
  `gpu_percent` decimal(5,2),
  `gpu_mem_free` decimal(10,1),
  `gpu_mem_total` decimal(10,1),
  `gpu_mem_used` decimal(10,1),
  `gpu_temp` decimal(4,1)
);

CREATE TABLE `client` (
    `id` integer PRIMARY KEY AUTO_INCREMENT
);

CREATE INDEX `monitoring_index_0` ON `monitoring` (`timestamp`);

CREATE INDEX `monitoring_index_1` ON `monitoring` (`client_id`);

ALTER TABLE `monitoring` ADD FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);