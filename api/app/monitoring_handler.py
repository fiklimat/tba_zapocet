from db_handler import DBHandler
from classes import Client, Monitoring
from datetime import datetime

def get_new_client_id(db: DBHandler) -> int:
    session = db.get_session()
    try:
        with session.begin():
            client = Client()
            session.add(client)
        return client.id
    except Exception as e:
        print(e)
        session.rollback()
    finally:
        session.close()
        

def get_client_ids(db: DBHandler) -> list:
    session = db.get_session()
    try:
        client_ids = session.query(Client.id).all()
        return [client_id[0] for client_id in client_ids]
    except Exception as e:
        print(e)
    finally:
        session.close()

def get_monitorings(db: DBHandler, ci: int = None, f: str = None, t: str = None) -> list:
    session = db.get_session()
    query = session.query(Monitoring)

    if f is not None and f != "" and t is not None and t != "":
        f = datetime.strptime(f,"%Y-%m-%dT%H:%M")
        t = datetime.strptime(t,"%Y-%m-%dT%H:%M")
        query = query.filter(Monitoring.timestamp.between(f,t))
    if ci is not None:
        try: 
            ci = int(ci)
            query = query.filter(Monitoring.client_id == ci)
        except Exception:
            pass
    try:
        monitorings = query.all()
        return monitorings
    except Exception as e:
        print(e)
    finally:
        session.close()

def insert_monitorings(db: DBHandler, monitorings: list) -> None:
    session = db.get_session()
    try:
        with session.begin():
            if session.get(Client, monitorings[0].client_id) is None:
                client_id = get_new_client_id(db)
                for monitoring in monitorings:
                    monitoring.client_id = client_id
            session.add_all(monitorings)
    except Exception as e:
        session.rollback()
        print(e)
    finally:
        session.close()