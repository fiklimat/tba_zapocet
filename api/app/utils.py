from pathlib import Path
from classes import Monitoring
import json
from datetime import datetime
import pandas as pd
from io import StringIO

def loadConfig(name: str) -> dict:
    path = Path(__file__).parent.joinpath(f"config/{name}")
    with open(path,"r") as file:
        return json.load(file)
    
def monitoring_from_json(data: dict) -> Monitoring:
    return Monitoring(
        client_id = data['client_id'],
        timestamp = datetime.strptime(data['timestamp'],"%Y-%m-%d %H:%M:%S"),
        cpu_percent = data['cpu_percent'],
        cpu_frequency = data['cpu_frequency'],
        mem_available = data['mem_available'],
        mem_free = data['mem_free'],
        mem_percent = data['mem_percent'],
        mem_total = data['mem_total'],
        mem_used = data['mem_used'],
        net_bytes_recv = data['net_bytes_recv'],
        net_bytes_sent = data['net_bytes_sent'],
        net_packets_recv = data['net_packets_recv'],
        net_packets_sent = data['net_packets_sent'],
        battery_percent = data['battery_percent'],
        battery_plugged = data['battery_plugged'],
        batery_secsleft = data['batery_secsleft'],
        gpu_percent = data['gpu_percent'],
        gpu_mem_free = data['gpu_mem_free'],
        gpu_mem_total = data['gpu_mem_total'],
        gpu_mem_used = data['gpu_mem_used'],
        gpu_temp = data['gpu_temp']
)

def csv_from_monitorings(monitorings: list):
    dicts = [mon.__dict__ for mon in monitorings]
    df = pd.DataFrame(dicts)
    delete = ['_sa_instance_state','client']
    columns_to_drop = [col for col in delete if col in df.columns]
    df = df.drop(columns=columns_to_drop)
    csv_string = df.to_csv(index=False)
    return csv_string.encode('utf-8')
    
def monitorings_from_csv(csv_bytes: bytes) -> list:
    df = pd.read_csv(StringIO(csv_bytes.decode('utf-8')))
    return [monitoring_from_df_row(row[1]) for row in df.iterrows()]


def monitoring_from_df_row(row) -> Monitoring:
    return Monitoring(
        timestamp = row['timestamp'],
        client_id = row['client_id'],
        cpu_percent = row['cpu_percent'],
        cpu_frequency = row['cpu_frequency'],
        mem_available = row['mem_available'],
        mem_free = row['mem_free'],
        mem_percent = row['mem_percent'],
        mem_total = row['mem_total'],
        mem_used = row['mem_used'],
        net_bytes_recv = row['net_bytes_recv'],
        net_bytes_sent = row['net_bytes_sent'],
        net_packets_recv = row['net_packets_recv'],
        net_packets_sent = row['net_packets_sent'],
        battery_percent = row['battery_percent'],
        battery_plugged = row['battery_plugged'],
        batery_secsleft = row['batery_secsleft'],
        gpu_percent = row['gpu_percent'],
        gpu_mem_free = row['gpu_mem_free'],
        gpu_mem_total = row['gpu_mem_total'],
        gpu_mem_used = row['gpu_mem_used'],
        gpu_temp = row['gpu_temp'],
    )