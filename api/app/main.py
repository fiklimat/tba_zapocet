from fastapi import FastAPI, Response, UploadFile, File, Query
# from fastapi.middleware.cors import CORSMiddleware
from db_handler import DBHandler
from classes import create_database
import utils as ut
import monitoring_handler as mh
import uvicorn

app = FastAPI(title="API")
db = DBHandler()
create_database(db)
# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=["*"],  # Replace "*" with the appropriate origins
#     allow_credentials=True,
#     allow_methods=["GET", "POST", "PUT", "DELETE"],  # Add the allowed HTTP methods
#     allow_headers=["*"],  # Replace "*" with the appropriate headers
# )

@app.get("/client_id/new", summary="Create new client and get his Id", responses={
    200: {
        "description": "Successful Response",
        "content": {
            "application/json": {
                "example": {
                    "id": 0
                }
            }
        }
    }
})
async def get_new_client_id():
    client_id = mh.get_new_client_id(db)
    return {'id': client_id}

@app.get("/client_id", responses={
    200: {
        "description": "Successful Response",
        "content": {
            "application/json": {
                "example": [
                    0
                ]
            }
        }
    }
})
async def get_client_ids():
    client_ids = mh.get_client_ids(db)
    return client_ids

@app.get("/monitoring/csv", summary="Download monitoring data as CSV", response_class=Response(media_type="text/csv"), responses={
    200: {
        "description": "Successful Response",
        "content": {
            "text/csv": {
                "example": """net_bytes_recv,gpu_percent,id,net_bytes_sent,gpu_mem_free,cpu_frequency,net_packets_recv,gpu_mem_total,timestamp,mem_available,net_packets_sent,gpu_mem_used,cpu_percent,mem_free,battery_percent,gpu_temp,mem_percent,battery_plugged,mem_total,batery_secsleft,client_id,mem_used
2702,0.00,1,0,5156.0,2808.00,8,6144.0,2024-05-22 23:49:31,4632,0,874.0,9.90,4632,100,53.0,73.00,True,17134,-2,1,12503"""
            }
        }
    }
})
async def get_monitoring_csv(ci: str = Query(default=None, description="Client id"), 
                             f: str = Query(default=None, description="Start datetime filter [format: Y-m-dTH:M]"), 
                             t: str = Query(default=None, description="End datetime filter [format: Y-m-dTH:M]")):
                             
    monitorings = mh.get_monitorings(db,ci,f,t)
    csv_bytes = ut.csv_from_monitorings(monitorings)
    response = Response(content=csv_bytes, media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=data.csv"
    response.headers["Content-Type"] = "text/csv"   
    return response
    
@app.post("/monitoring/csv", summary="Upload monitoring data in CSV")
async def post_monitoring_csv(csv_file: UploadFile = File(description="CSV file with monitoring data")):
    if not csv_file.filename.endswith('.csv'):
        return {"error": "Only CSV files are allowed"}
    contents = await csv_file.read()
    monitorings = ut.monitorings_from_csv(contents)
    mh.insert_monitorings(db, monitorings)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
    # uvicorn.run(app, host="localhost", port=8000)