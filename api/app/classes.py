from sqlalchemy import Column, Integer, DateTime, Boolean, Numeric, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

class Monitoring(Base):
    __tablename__ = 'monitoring'
    id = Column(Integer, primary_key=True, autoincrement=True)
    timestamp = Column(DateTime, nullable=False)
    client_id = Column(Integer, ForeignKey('client.id'), nullable=False)
    cpu_percent = Column(Numeric(5, 2))
    cpu_frequency = Column(Numeric(10, 2))
    mem_available = Column(Integer)
    mem_free = Column(Integer)
    mem_percent = Column(Numeric(5, 2))
    mem_total = Column(Integer)
    mem_used = Column(Integer)
    net_bytes_recv = Column(Integer)
    net_bytes_sent = Column(Integer)
    net_packets_recv = Column(Integer)
    net_packets_sent = Column(Integer)
    battery_percent = Column(Integer)
    battery_plugged = Column(Boolean)
    batery_secsleft = Column(Integer)
    gpu_percent = Column(Numeric(5, 2))
    gpu_mem_free = Column(Numeric(10, 1))
    gpu_mem_total = Column(Numeric(10, 1))
    gpu_mem_used = Column(Numeric(10, 1))
    gpu_temp = Column(Numeric(4, 1))
    client = relationship('Client', back_populates='monitorings', lazy="joined")

class Client(Base):
    __tablename__ = 'client'
    id = Column(Integer, primary_key=True, autoincrement=True)
    monitorings = relationship('Monitoring', back_populates='client')

def create_database(db) -> None:
    Base.metadata.create_all(db.engine)
