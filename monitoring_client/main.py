import pika
import json
import psutil
import GPUtil
from pathlib import Path
from datetime import datetime
import requests

def getClientId(rabbitmq_config: dict, api_config: dict) -> int:
    if rabbitmq_config['client_id'] is not None:
        return rabbitmq_config['client_id']
    
    response = requests.get(f"{api_config['url']}client_id/new")
    id = response.json()['id']
    writeIdIntoRabbitConfig(id)
    return id

def loadConfig(name: str) -> dict:
    path = Path(__file__).parent.joinpath(f"config/{name}")
    with open(path,"r") as file:
        return json.load(file)
    
def writeIdIntoRabbitConfig(id: int):
    path = Path(__file__).parent.joinpath("config/rabbitmq_config.json")
    with open(path,"r") as file: 
        file_dict = json.load(file)
        file_dict['client_id'] = id
    with open(path,"w") as file:
        json.dump(file_dict, file)

if __name__ == "__main__":
    rabbitmq_config = loadConfig("rabbitmq_config.json")
    api_config = loadConfig("api_config.json")

    connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_config['host'],port=rabbitmq_config['port']))
    channel = connection.channel()
    channel.queue_declare(queue=rabbitmq_config['queue'])

    client_id = getClientId(rabbitmq_config, api_config)
    net_old = psutil.net_io_counters()

    while True:
        cpu_percent = psutil.cpu_percent(interval=1)
        cpu_freq = psutil.cpu_freq().current
        mem = psutil.virtual_memory()
        # sm = psutil.swap_memory()
        net = psutil.net_io_counters()
        battery = psutil.sensors_battery()
        gpu = GPUtil.getGPUs()[0] # Pouze pro Windows
        processes = len(psutil.pids())

        data = {
            'client_id': client_id,
            'timestamp': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'cpu_percent': cpu_percent,
            'cpu_frequency': cpu_freq,
            'mem_available': round(mem.available/1000000),
            'mem_free': round(mem.free/1000000),
            'mem_percent': mem.percent,
            'mem_total': round(mem.total/1000000),
            'mem_used': round(mem.used/1000000),
            'net_bytes_recv': net.bytes_recv - net_old.bytes_recv,
            'net_bytes_sent': net.bytes_sent - net_old.bytes_sent,
            'net_packets_recv': net.packets_recv - net_old.packets_recv,
            'net_packets_sent': net.packets_sent - net_old.packets_sent,
            'battery_percent': battery.percent,
            'battery_plugged': battery.power_plugged,
            'batery_secsleft': battery.secsleft,
            'gpu_percent': gpu.load * 100,
            'gpu_mem_free': gpu.memoryFree,
            'gpu_mem_total': gpu.memoryTotal,
            'gpu_mem_used': gpu.memoryUsed,
            'gpu_temp': gpu.temperature
        }
        net_old = net

        channel.basic_publish(exchange='', routing_key=rabbitmq_config['queue'], body=json.dumps(data, indent=2))
        print("sent")
        